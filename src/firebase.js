import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

// TODO: Replace the following with your app's Firebase project configuration
// See: https://firebase.google.com/docs/web/learn-more#config-object
const firebaseConfig = {
    apiKey: "AIzaSyBINDB4pR9Pb80-MirQ5e45528EyHUz3Dc",
    authDomain: "devcamp-r2225.firebaseapp.com",
    projectId: "devcamp-r2225",
    storageBucket: "devcamp-r2225.appspot.com",
    messagingSenderId: "339507008050",
    appId: "1:339507008050:web:464ca820faa53078fb222b"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Firebase Authentication and get a reference to the service
const auth = getAuth(app);

export default auth;