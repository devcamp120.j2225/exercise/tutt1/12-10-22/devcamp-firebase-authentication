import logo from './logo.svg';
import './App.css';

import auth from './firebase';
import { GoogleAuthProvider, onAuthStateChanged, signInWithPopup, signOut } from 'firebase/auth';
import { useEffect, useState } from 'react';

const provider = new GoogleAuthProvider();

function App() {
  const [user, setUser] = useState(null);

  const loginGoogle = () => {
    signInWithPopup(auth, provider)
    .then((result) => {
      console.log(result);
      setUser(result.user);
    })
    .catch((error) => {
      console.log(error);
      setUser(null);
    })
  }

  const logoutGoogle = () => {
    signOut(auth)
    .then(() => {
      console.log('Sign Out');
      setUser(null);
    })
    .catch ((error) => {
      console.log(error);
      setUser(null);
    })
  }

  useEffect(() => {
    onAuthStateChanged(auth, (result) => {
       if (result) {
        console.log(result);
        setUser(result);
       } else {
        console.log(result);
        setUser(null);
       }
    })
  }, [])
  
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {
          user ?
          <>
            <img src={user.photoURL} />
            <p>Hello {user.displayName}</p>
            <button onClick={logoutGoogle}>Signout</button>
          </>
          :
          <>
            <p>Please signin</p>
            <button onClick={loginGoogle}>Signin with Google</button>          
          </>
        }
      </header>
    </div>
  );
}

export default App;